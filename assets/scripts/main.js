/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                // JavaScript to be fired on all pages

                $(document).foundation(); // Foundation JavaScript

            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.





// ====================== smooth scroll anchor ================

// jQuery(function() {
//   jQuery('.scrolldown').click(function() {
//     if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
//       var target = jQuery(this.hash);
//       target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
//       if (target.length) {
//         jQuery('html,body').animate({
//           scrollTop: target.offset().top - 80
//         }, 1000);
//         return false;
//       }
//     }
//   });
// });

// ====================== end smooth scroll anchor ================



jQuery(document).ready(function(){      // global document.ready



});

jQuery(document).ready(function(){
    // wordpress admin panel
    jQuery('html').attr('style', 'margin-top: 0!important');
    jQuery('#wpadminbar').addClass('overflow');
    var hide;
    jQuery('#wpadminbar').on('mouseover', function(){
        setTimeout(function(){
            jQuery('#wpadminbar').removeClass('overflow');
        },1000);
        if(!jQuery('#wpadminbar').hasClass('open')){
            jQuery('#wpadminbar').addClass('open overflow');
        } else{
            clearTimeout(hide);
        }
    });
    jQuery('#wpadminbar').on('mouseleave', function(){
        hide = setTimeout(function(){
            jQuery('#wpadminbar').addClass('overflow');
            jQuery('#wpadminbar').removeClass('open');
        },2000);
    });
    // end wordpress admin panel
});


var canvas = document.getElementById("network");
var context = canvas.getContext("2d");
var countX, countY;
var yNum = [];
var xNum = [];

drawTemplate(context);

window.onresize=function() {
    drawTemplate(context);
};

function circle(x, y, r, b, pi, bool, opacity) {
    context.beginPath();
    context.arc(x, y, r, b, pi, bool);
    context.closePath();
    if (opacity=='dots') {
        context.fillStyle = "rgba(255,255,255,0.8)";
    } else {
        context.fillStyle = "rgba(255,255,255,0)";
    }

    context.shadowBlur = 0;
    context.fill();
}

function drawTemplate(context) {
    jQuery('.main-content').css('height', window.innerHeight - 90);
    jQuery('.background-opacity').css({"height": window.innerHeight - 90, "width": '100%'});
    jQuery('.projects-slider').css('height', window.innerHeight - 90);
    jQuery('.slide-item').css('height', window.innerHeight - 90);
    jQuery('.slider-background').css('height', window.innerHeight - 90);

    // DOTED LAYOUT
    var content = document.getElementById("content");
    var dotWidth  = content.offsetWidth;
    var dotHeight = jQuery('.background-opacity').height();
    var x = 2;
    var y = 2;

    context.canvas.width  = dotWidth;
    context.canvas.height = dotHeight;

    var circles = [];
    var width = document.body.clientWidth;
    var height = screen.height - 90;
    countX = Math.floor(dotWidth/80) + 1;
    countY = Math.floor(dotHeight/85) + 1;

    //    settings for transparent dots
    var page = jQuery("#content").data("page");
    if (page == 'index') {
        if (countY % 2 == 0) {
            yNum = [countY/2, countY/2+1];
            var start = countX/2 -2;
        } else {
            yNum = [countY/2+1];
        }
        if (countX % 2 == 0) {
            var start = countX/2 -2;
            xNum = [start, start+1, start+2, start+3, start+4, start+5];
        } else {
            start = Math.floor(countX/2)-1;
            xNum = [start, start+1, start+2, start+3, start+4];
        }
    }
    //    end settings for transparent dots

    for(var i = 1; i <= countX; i++) {
        for(var j = 1; j <= countY; j++) {
            if(jQuery.inArray(j, yNum) !== -1 && jQuery.inArray(i, xNum) !== -1) {
                circle(x, y, 2, 0, Math.PI*2, false, true);
            } else {
                circle(x, y, 2, 0, Math.PI*2, false, 'dots');
            }
            y += 84;
            if(j == countY) {
                y = 2;
            }
        }
        x += 79;
    }

    //END OF DOTED LAYOUT

    var paddingLeft = (window.innerWidth - (countX-1)*79)/2;
    var paddingTop =(window.innerHeight - countY*84)/2 - 2;
    jQuery('.background-opacity').css({"padding-left": paddingLeft, "padding-top": paddingTop});
    jQuery('.contacts').css({"left":paddingLeft + 1, "top": paddingTop+1});
    jQuery('.advantages-wrapper').css({"top":canvas.offsetTop+1, "right": window.innerWidth - (countX-1)*79 -paddingLeft - 3});
    jQuery('.advantages-wrapper.adv_project').css({"top":canvas.offsetTop+1, "right": window.innerWidth - (countX-3)*79 -paddingLeft - 3});
    jQuery('.adv_main').css({"top":canvas.offsetTop+1, "right": window.innerWidth - (countX-1)*79 -paddingLeft - 3});
    jQuery('.adv_projects .adv_main').css({"top":0, "right": -1});
    jQuery('.adv_main.proj').css({"top":canvas.offsetTop+1, "right": window.innerWidth - (countX-3)*79 -paddingLeft - 3});

    //HOME PAGE BLOCKS
    if(window.innerWidth <= 768) {
        jQuery('.blocks').css("height", 85);
        var elLeft = canvas.offsetLeft+1 + 79 * Math.floor(countX/2)-79;
        var divLeft = elLeft - 79;
        jQuery('span.buy').css({"top": canvas.offsetTop - 2 - 168 + 84 * (countY-1), "left":elLeft-2 + 79});
        jQuery('div.buy').css({"top": canvas.offsetTop + 1 - 168 + 84 * (countY-1),"left":divLeft});

        jQuery('span.commerce').css({"top": canvas.offsetTop -2 + 84 * (countY-4), "left":elLeft-2});
        jQuery('div.commerce').css({"top": canvas.offsetTop + 1 + 84 * (countY-4),"left":divLeft});

        jQuery('span.development').css({"top": canvas.offsetTop - 2 + 84 * (countY-1), "left":elLeft-2});
        jQuery('div.development').css({"top": canvas.offsetTop - 83 + 84 * (countY-1),"left":divLeft});

        jQuery('span.build').css({"top": canvas.offsetTop -2 - 168 + 84 * (countY-3), "left":elLeft-2 + 158});
        jQuery('div.build').css({"top": canvas.offsetTop + 1 - 84 + 84 * (countY-3)-84,"left":divLeft});

    } else {
        jQuery('span.buy').css({"top":canvas.offsetTop-2 + 84 * Math.floor(countY*0.4), "left":canvas.offsetLeft-2 + 79 * Math.floor(countX/4)});
        jQuery('div.buy').css({"top":canvas.offsetTop+1 + 84 * Math.floor((countY)*0.4)-168,"left":canvas.offsetLeft+1 + 79 * Math.floor(countX/4)});

        jQuery('span.commerce').css({"top":canvas.offsetTop-2 + 84 * Math.floor(countY-3), "left":canvas.offsetLeft-2 + 79 * Math.floor(countX/5)});
        jQuery('div.commerce').css({"top":canvas.offsetTop+1 + 84 * Math.floor(countY-3)-84,"left":canvas.offsetLeft+1 + 79 * Math.floor(countX/5)});

        jQuery('span.development').css({"top":canvas.offsetTop-2 + 84 * Math.floor(countY*0.25), "left":canvas.offsetLeft-2 + 79 * Math.floor(countX*0.6)});
        jQuery('div.development').css({"top":canvas.offsetTop+1 + 84 * Math.floor(countY*0.25),"left":canvas.offsetLeft+1 + 79 * Math.floor(countX*0.6)});

        jQuery('span.build').css({"top":canvas.offsetTop-2 + 84 * Math.floor(countY-2), "left":canvas.offsetLeft-2 + 79 * Math.floor(countX/3*2)});
        jQuery('div.build').css({"top":canvas.offsetTop+1 + 84 * Math.floor(countY-2)-84,"left":canvas.offsetLeft+1 + 79 * Math.floor(countX/3*2) - 79});
    }

    //NAVIGATION

    jQuery('.menu li').css("width", window.innerWidth/10);
    jQuery('.logo').css("width", window.innerWidth - jQuery('.menu li').width() * 7 - jQuery('.social').width()-2);
    jQuery('.toogle').click(function() {
        jQuery('ul.menu').toggleClass('dropup');
    });

    //SPARKLE DOTS

    jQuery('span.sparkle').css({"top":canvas.offsetTop-2 + 84 * Math.floor(countY/4), "left":canvas.offsetLeft-2 + 79 * Math.floor(countX/3)});
    window.setInterval(function(){
        var randX = Math.floor(Math.random() * ((countX-3)+1) + 1);
        var randY = Math.floor(Math.random() * ((countY-2)+1) + 1);
        jQuery('span.sparkle').css({"top":canvas.offsetTop-2 + 84 * randY, "left":canvas.offsetLeft-2 + 79 * randX});
    }, 4000);

    if (window.innerWidth <= 768) {
        jQuery('.page_info').css({"top":canvas.offsetTop+1 + 84 * 2, "left":canvas.offsetLeft+1,
            "right":window.innerWidth - (countX-1)*79 -paddingLeft - 3, "height": (countY-3) * 84 + 1});
        jQuery('.project-navigation').css({"left":paddingLeft + 1,
            "top": paddingTop+1 + 85, "width": 79 * 4 + 2});
    } else if (window.innerWidth <= 992) {
        jQuery('.page_info').css({"top":canvas.offsetTop+1 + 84 * 2, "left":canvas.offsetLeft+1 + 79 * 2,
            "right":window.innerWidth - (countX-2)*79 -paddingLeft - 3, "height": (countY-3) * 84 + 1});
        jQuery('.project-navigation').css({"left":paddingLeft + 1 + 3*79, "top": paddingTop+1 + 84, "width": 79 * 5 + 2});
    } else if (window.innerWidth <= 1380) {
        jQuery('.page_info').css({"top":canvas.offsetTop+1 + 84 * 2, "left":canvas.offsetLeft+1 + 79 * 3,
            "right":window.innerWidth - (countX-3)*79 -paddingLeft - 3, "height": (countY-4) * 84 + 1});
        jQuery('.project-navigation').css({"left":paddingLeft + 1 + 3*79, "top": paddingTop+1 + 84, "width": 79 * 6 + 2});
    } else {
        jQuery('.page_info').css({"top":canvas.offsetTop+1 + 84 * 2, "left":canvas.offsetLeft+1 + 79 * 3,
            "right":window.innerWidth - (countX-3)*79 -paddingLeft - 3, "height": (countY-4) * 84 + 1});
        jQuery('.project-navigation').css({"left":paddingLeft + 1 + 3*79, "top": paddingTop+1, "width": 79 * 6 + 2});
    }

    if (window.innerWidth <= 560) {
        jQuery('.project-info').css({
            "top": canvas.offsetTop + 1 + 84 * 2, "left": canvas.offsetLeft + 1,
            "right": window.innerWidth - (countX - 1) * 79 - paddingLeft - 4, "height": (countY - 3) * 84 + 1
        });
    } else if(window.innerWidth <= 768) {
        jQuery('.project-info').css({
            "top": canvas.offsetTop + 1 + 84 * 2, "left": canvas.offsetLeft + 1 + 79,
            "right": window.innerWidth - (countX - 2) * 79 - paddingLeft - 4, "height": (countY - 3) * 84 + 1
        });
    } else if (window.innerWidth <= 992) {
        jQuery('.project-info').css({
            "top": canvas.offsetTop + 1 + 84 * 2, "left": canvas.offsetLeft + 1 + 79,
            "right": window.innerWidth - (countX - 1) * 79 - paddingLeft - 4, "height": (countY - 3) * 84 + 1
        });
    } else if (window.innerWidth <= 1200) {
        jQuery('.project-info').css({
            "top": canvas.offsetTop + 1 + 84 * 2, "left": canvas.offsetLeft + 1 + 79 * 2,
            "right": window.innerWidth - (countX - 2) * 79 - paddingLeft - 4, "height": (countY - 3) * 84 + 1
        });
    } else {
        jQuery('.project-info').css({
            "top": canvas.offsetTop + 1 + 84 * 2, "left": canvas.offsetLeft + 1 + 79 * 3,
            "right": window.innerWidth - (countX - 3) * 79 - paddingLeft - 4, "height": (countY - 3) * 84 + 1
        });
    }

    var info_width = jQuery('.page_info').width();


    if (countY <= 6) {
        jQuery('.vac').css("height", 253);
        jQuery('.list-item').css('height', 85);
    } else {
        jQuery('.vac').css("height", 505);
        jQuery('.list-item').css('height', 169);
    }
    if (window.innerWidth <= 417) {
        jQuery('.vacancies-content').css("width", info_width - 120);
        jQuery('.slider-address').css( "width", 79 * 2 + 1);
        jQuery('.close').css({"top":canvas.offsetTop+1 + 85, "right": window.innerWidth - (countX-1)*79 -paddingLeft - 3 + 80});
    } else {
        jQuery('.close').css({"top":canvas.offsetTop+1, "right": window.innerWidth - (countX-1)*79 -paddingLeft - 3});
        jQuery('.vacancies-content').css("width", info_width - 159);
        jQuery('.slider-address').css( "width", 79 * 3 + 1);
    }

    //SETTINGS FOR SLICK SLIDER
    jQuery('.simple-slider').slick({
        nextArrow: '<i class="fa fa-arrow-right"></i>',
        prevArrow: '<i class="fa fa-arrow-left"></i>',
    });
    jQuery('.projects-slider').slick({
        dots: true,
        arrows: false,
    });
    if (window.innerHeight <= 667) {
        jQuery('.slick-dots').css({"left":paddingLeft + 1, "top": paddingTop+1 + 84 * Math.floor(countY-2)});
    } else {
        jQuery('.slick-dots').css({"left":paddingLeft + 1, "top": paddingTop+1 + 84 * Math.floor(countY-3)});
    }
    jQuery('.slider-info').css({"left":paddingLeft + 1, "top": paddingTop+1 + 84 * Math.floor(countY*0.4)});
    jQuery('.slider-title').css("width", 79 * Math.floor(countX*0.4) + 1);
}

//ADVANTAGES

jQuery('.advantages-description').css("right", -640 + 'px');
jQuery('.advantages-description.proj').css("right", -480 + 'px');
jQuery('.adv_main').hover(function() {
    jQuery('.advantages-description').css("right", 0);
});
jQuery('.advantages-wrapper').mouseleave(function() {
    jQuery('.advantages-description').css("right", -640 + 'px');
    jQuery('.advantages-description.proj').css("right", -480 + 'px');
});

//BLOCKS ANIMATION
var temporaryVar = 1;
function doAnimation(className) {
    jQuery('.'+className+' .line-one').css('animation-name', 'line-one-blocks');
    jQuery('.'+className+' .line-two').css('animation-name', 'line-two-blocks');
    jQuery('.'+className+' .line-three').css('animation-name', 'line-three-blocks');
    jQuery('.'+className+' .line-four').css('animation-name', 'line-four-blocks');
    jQuery('.'+className+' .block-content').css('animation-name', 'block-content');
    setTimeout(function() {
        jQuery('.site-title').css("opacity", 0);
        reDrowDots(temporaryVar);
        temporaryVar++;
    }, 2000);
}
function reDrowDots(temporaryVar) {
    if (temporaryVar == 1) {
        var x = 2;
        var y = 2;
        for(var i = 1; i <= countX; i++) {
            for(var j = 1; j <= countY; j++) {
                if(jQuery.inArray(j, yNum) !== -1 && jQuery.inArray(i, xNum) !== -1) {
                    circle(x, y, 2, 0, Math.PI*2, false, 'dots');
                }
                y += 84;
                if(j == countY) {
                    y = 2;
                }
            }
            x += 79;
        }
    }

}

var services = ['build', 'commerce', 'buy', 'development'];
var animationDelay = [13000, 17000, 15000, 19000];
jQuery.each( services, function( key, name ) {
    setTimeout(function() {
        doAnimation(name);
    }, animationDelay[key]);
});

jQuery('#content span.dot').hover(function() {
    var className = jQuery(this).attr('class').replace(/dot /g, '');
    doAnimation(className);
});

jQuery('.slick-slider div, .slick-arrow').hover(function() {
    jQuery('.slick-arrow').css("opacity", 1);
});

jQuery('.slick-slider div, .slick-arrow').mouseleave(function() {
    jQuery('.slick-arrow').css("opacity", 0);
});

jQuery(".list-item a").click(function() {
    var id = jQuery(this).attr('href');
    jQuery('.vacancies-content').animate({scrollTop: jQuery(id).offset().top }, 500);
});
